var express = require("express");
var bodyParser = require("body-parser");
const csp = require('express-csp-header');
var routes = require("./web/rest.ts");
var index = require("./web/index.js");
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(csp({
//     policies: {
//         'default-src': ['bootstrapcdn.com', 'telegram.org', 'jquery.com', 'mapbox.com'],
//         'script-src': ['bootstrapcdn.com', 'telegram.org', 'jquery.com', 'mapbox.com'],
//     }
// }));
app.use(express.static('./web/assets'));
app.use(express.static('./web/assets/flags'));

routes(app);
index(app);

var server = app.listen(3000, function() {
    console.log("app running on port.", server.address().port);
});