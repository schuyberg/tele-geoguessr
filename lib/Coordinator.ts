const inside = require('point-in-geopolygon');
const cnt = require('../resources/countries.json');
const panorama = require('google-panorama-by-location/node')
const request = require("request-promise");

import { getLogger } from './Logger';
import { Area, Point } from './Types';

const log = getLogger('Coordinator');

export class Coordinator {
    private slovenia = this.createArea(16.5, 46.9, 13.3, 45.5, ['Slovenia'], [], 50, 'Slovenia')
    public world = this.createArea(180, 90, -180, -90, [], [], 25000, 'World')
    private fetching = false;
    
    constructor (private db) {}

    start() {
        setInterval(this.generatePeriodically.bind(this), 10000);
    }

    async getUnusedPoints(type = 'World') {
        return await this.db.prepare(`SELECT * from points 
        where challengeId is null and challengeType = ? order by id asc`)
        .all(type) as Point[];
    }

    async generatePeriodically() {
        if (this.fetching) return;
        this.fetching = true;

        const worldPoints = await this.getUnusedPoints();
        
        if (worldPoints.length < 50) {
            const coordsW = await this.generateCoordinates(50 - worldPoints.length, this.world)
            await this.insertCoordinates(coordsW);
        }

        const sloPoints = await this.getUnusedPoints('Slovenia');
        
        if (sloPoints.length < 50) {
            const coordsS = await this.generateCoordinates(50 - sloPoints.length, this.slovenia)
            await this.insertCoordinates(coordsS);
        }

        this.fetching = false;
    }

    createArea(maxLat: number, maxLon: number, minLat: number, minLon: number,
        namesInc: Array<String> = [], namesExc: Array<String> = [], radius, name: string) {
        return {
            gpsGenerator: function() {
                return [((Math.random() * (maxLat - minLat)) + minLat).toFixed(8), ((Math.random() * (maxLon - minLon)) + minLon).toFixed(8)]; 
            },
            namesInc,
            namesExc,
            radius,
            name
        } as Area
    }

    async generateCoordinates(n = 5, area) {
        const results = []
        let nonIncluded = 0, excluded = 0, notFound = 0, notValid = 0
        const start = Date.now()

        for (let i = 0; i < n; i++) {
            const loc = area.gpsGenerator();
            const res = inside.feature(cnt, loc);

            if (res == -1) {
                notFound += 1
                i -= 1;
                continue
            }

            if (area.namesInc.length > 0 && !area.namesInc.includes(res["properties"]["ADMIN"])) {
                nonIncluded += 1
                i -= 1
                continue
            }

            if (area.namesExc.length > 0 && area.namesExc.includes(res["properties"]["ADMIN"])) {
                excluded += 1
                i -= 1
                continue
            }


            const valid = await this.findOnGoogle(loc[1], loc[0], Math.random() > 0.8 ? null : "outdoor", area.radius);
            if (valid.status != "OK") {
                notValid += 1
                i -= 1
                continue
            } else {
                var point: Point = {
                    country: res["properties"]["ADMIN"],
                    short: res["properties"]["ISO_A3"],
                    lat: valid.location.lat,
                    lon:  valid.location.lng, 
                    type: valid.copyright.includes("Google") ? 'google' : 'user',
                    id: valid.pano_id,
                    challengeType: area.name
                }

                results.push(point)
            }
        }

        log.info(`Generated ${n} points in ${(Date.now() - start) / 1000}s.
            Not found: ${notFound} Non included: ${nonIncluded} Excluded: ${excluded} Non valid: ${notValid}`)

        return results;
    }

    async insertCoordinates(points: Array<Point>) {
        for (var point of points) {
            await this.db.prepare(`INSERT INTO points (challengeId, country, lat, lon, type, googleId, short, challengeType) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`)
                .run(point.challengeId, point.country, point.lat, point.lon, point.type, point.id, point.short, point.challengeType);
        }

        return 
    }

    async findOnGoogle(lat, long, source, radius = 50000) {
        var url = 'https://maps.googleapis.com/maps/api/streetview/metadata?location='+lat+','+long+'&key=AIzaSyCosgpf_SGxSMZ57p8MrkAwSgDzylHhYw0'
        
        if (radius != 0) url += "&radius=" + radius
        if (source) url += "&source=" + source
        
        const res = await request(url);
        return JSON.parse(res);
    }
}





