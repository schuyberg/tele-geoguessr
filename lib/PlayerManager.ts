import { PlayerChallenge, Player } from './Types';
import { getLogger } from './Logger';

const log = getLogger("PlayerManager");

export class PlayerManager {
    constructor (private db) {}
    
    getPlayerById = async (id: any) => {
        const allPlayers = await this.db.prepare(`select *, ROW_NUMBER () OVER ( ORDER BY elo desc) rank from players`).all();
        return allPlayers.find(player => player.id == id);
    }

    getPlayerByName = async (name: any) => {
        return this.db.prepare(`select * from players where tName = ?`).get(name) as Player;
    }

    addPlayer = async (tName: string, gName: string, email: string, avatar: string) => {
        return this.db.prepare(`INSERT INTO players (tName, gName, email, avatar) VALUES (?, ?, ?, ?);`).run(tName, gName, email, avatar);
    }
    
    updateAvatar = async (tName: string,  avatar: string) => {
        return this.db.prepare(`update players set avatar = ? where tName = ?`).run(avatar, tName);
    }

    removePlayer = async (tName: string) => {
        return this.db.prepare(`DELETE FROM players where tName = ?;`).run(tName);
    }

    insertScore = async (challengeId: string, playerId: string, geoId: string, score: string) => {
        await this.db.prepare(`INSERT INTO playerScores (challengeId, playerId, geoId, score) VALUES (?, ?, ?, ?);`)
            .run(challengeId, playerId, geoId, score);

        const scores = await this.db.prepare(`select * from playerScores where challengeId = ? order by score desc`)
            .all(challengeId);
        
        const orderedIds = scores.map(playerId => playerId);
      
        const place = orderedIds.findIndex(x => x.playerId == playerId);
        return place;
    }

    getHighScore = async () => {
        const players = await this.db.prepare(`
            SELECT * from players p join playerScores ps on p.id = ps.playerId
            where ps.fetchTime > date('now', '-14 days') 
            group by p.id order by elo DESC
        `).all() as Player[];
        for (const player of players) {
            const extra = await this.db.prepare(`SELECT count(*) as matchCount, 
                max(score) as maxScore from playerScores where playerId = ?`).get(player.id);
            const ratingChange = await this.db.prepare(`SELECT ratingChange from playerScores 
                where playerId = ? and ratingChange is not null order by fetchTime 
                desc limit 1`).get(player.id);

            player.maxScore = extra.maxScore;
            player.matchCount = extra.matchCount;

            if (ratingChange) {
                player.lastRatingChange = parseInt(ratingChange.ratingChange) > 0 
                ? `+` + ratingChange.ratingChange
                : ratingChange.ratingChange;
            }
        }

        return players;
    }

    getUserInfo = async (tName: string) => {
        const player = await this.db.prepare(`SELECT * from players where tName = ? order by elo DESC`)
            .get(tName) as Player;
        
        const matchHistory = await this.db.prepare(`SELECT geoId, score, ratingChange, fetchTime
            from playerScores where playerId = ? order by fetchTime desc LIMIT 5`)
            .all(player.id) as PlayerChallenge[];
        
        const extra = await this.db.prepare(`SELECT count(*) as matchCount, max(score) as maxScore from playerScores 
            where playerId = ?`).get(player.id);

        player.maxScore = extra.maxScore;
        player.matchCount = extra.matchCount;
        player.matchHistory = matchHistory;

        return player;
    }
}
