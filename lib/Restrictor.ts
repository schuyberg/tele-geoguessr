const config = require('config');

export class Restrictor {
    constructor(private db) {}
    
    static lastCommandsTime = {};
    spamFilterTime = config.get('bot.spamFilterTime');
    
    runSpamFilter = async (tName: string) => {
        const now = Date.now();
        let allow: boolean = false;

        if (Restrictor.lastCommandsTime[tName]) {
            if (Restrictor.lastCommandsTime[tName] < (now - this.spamFilterTime)) allow = true;
            else allow = false;
        } else allow = true;

        Restrictor.lastCommandsTime[tName] = now;

        return allow;
    }

    doesPlayerExist = async (tName: string) => {
        const player = await this.db.prepare(`SELECT tName FROM players WHERE tName = ?;`).get(tName);
        if (player) return true;
        return false;
    }
}