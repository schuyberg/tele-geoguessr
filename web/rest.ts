const Database = require('better-sqlite3');
const db = new Database('./tg.db', {});

import { RoundManager } from '../lib/RoundManager';
import { PlayerManager } from '../lib/PlayerManager';

const roundManager = new RoundManager(db);
const playerManager = new PlayerManager(db);

const challengeRoute = '/rest/userChallenge';
const userRoute = '/rest/user';

var rest = function(app) {
    app.get(`${challengeRoute}/:challengeId`, async function(req, res) {
        const points = await roundManager.getPointsForChallenge(req.params.challengeId);
        if (!points || points.length == 0) {
            res.status(500).send('No such challenge');
            return;
        }

        console.log(req.query.user)
        const player = await playerManager.getPlayerByName(req.query.user.first_name)
        if (!player) {
            res.status(500).send('Cannot find player');
            return
        }

        const allPlayerRounds = await roundManager.getRoundsInfo(req.params.challengeId);
        const playerRounds = allPlayerRounds.filter(round => round.playerId == player.id);
  
        let currentRound = playerRounds.length + 1

        const currentScore = playerRounds.reduce((res, round) => res + round.score, 0 );
        let currentHighscore = {}
        
        const filteredPlayerRounds = allPlayerRounds.filter(round => parseInt(round.roundId) < currentRound)
        for (let round of filteredPlayerRounds) {
            const player = await playerManager.getPlayerById(round.playerId);
            round.name = player.tName;
            round['rank'] = player.rank;
        }

        for (let playerRound of filteredPlayerRounds) {
            if (currentHighscore[playerRound.name])
                currentHighscore[playerRound.name].score += playerRound.score
            else {
                currentHighscore[playerRound.name] = {}
                currentHighscore[playerRound.name].score = playerRound.score           
                currentHighscore[playerRound.name].rank = playerRound['rank'];
            }

            if (parseInt(playerRound.roundId) === currentRound - 2) {
                currentHighscore[playerRound.name].lastScore = playerRound.score;
            }

            if (parseInt(playerRound.roundId) === currentRound - 1) {
                currentHighscore[playerRound.name].lat = playerRound.lat;  
                currentHighscore[playerRound.name].lon = playerRound.lon;  
            }
        }

        let sortedKeys = Object.keys(currentHighscore).sort(function(a,b){return currentHighscore[b].score-currentHighscore[a].score})
        const finalHighscore = {}
        for (let key of sortedKeys) {
            
            finalHighscore[key] = currentHighscore[key];
        }

        // first round! all we need is the first point... i think
        if (currentRound == 1) {
            res.status(200).send({ 
                points: [ points[0] ],
                currentRound,
                currentScore,
                currentHighscore: finalHighscore,
            });
        } else {
            res.status(200).send({ 
                points: points.slice(0, currentRound),
                rounds: playerRounds,
                currentRound,
                currentScore,
                currentHighscore: finalHighscore,
                filteredPlayerRounds
            });
        }
    });

    app.post(`${challengeRoute}/postResult`, async function(req, res) {
        const { roundId, challengeId, first_name, lat, lon } = req.body;
        const player = await playerManager.getPlayerByName(first_name)
        if (!player) res.status(500).send('Cannot find player');


        res.status(200).send(await roundManager.postResult(roundId, challengeId, player.id, lat, lon));
    });

    app.post(`${userRoute}/getUser`, async function(req, res) {
        const {id, first_name, photo_url, auth_date, hash } = req.body;
        let player = await playerManager.getPlayerByName(first_name)
        if (!player) {
            await playerManager.addPlayer(first_name, first_name, "", photo_url)
            player = await playerManager.getPlayerByName(first_name)
        } else if (!player.avatar) {
            await playerManager.updateAvatar(first_name, photo_url)
            player = await playerManager.getPlayerByName(first_name)
        }

        res.status(200).send(player);
    });
}

module.exports = rest;