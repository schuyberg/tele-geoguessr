const authenticate = () => {

}

const index = function(app) {
    app.get('/point*', function(req, res) {
        res.sendFile(__dirname + '/point.html');
    });

    app.get('/*.result', function(req, res) {
        res.sendFile(__dirname + '/result.html');
    });

    app.get('/([0-9]*$)', function(req, res) {
        res.sendFile(__dirname + '/index.html');
    });

    app.get('/error', function(req, res) {
        res.status(500).send('Errord. sorry');
    });
}

module.exports = index;